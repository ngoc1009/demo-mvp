
package com.example.ngocdroid.foodappdemomvpwithandroidx.view.detail;


import com.example.ngocdroid.foodappdemomvpwithandroidx.model.Meals;

public interface DetailView {

    void showLoading();
    void hideLoading();
    void setMeal(Meals.Meal meal);
    void onErrorLoading(String message);
}
