
package com.example.ngocdroid.foodappdemomvpwithandroidx.common;



import android.content.Context;
import android.content.DialogInterface;


import androidx.appcompat.app.AlertDialog;

import com.example.ngocdroid.foodappdemomvpwithandroidx.api.FoodApi;
import com.example.ngocdroid.foodappdemomvpwithandroidx.api.FoodClient;


public class Utils {

    public static FoodApi getApi() {
        return FoodClient.getFoodClient().create(FoodApi.class);
    }


    public static void showDialogMessage(Context context, String title, String message, int iconResId) {
        if (context != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setIcon(iconResId);
            builder.setCancelable(false);
            builder.setPositiveButton("Đóng", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }


}
