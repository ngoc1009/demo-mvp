
package com.example.ngocdroid.foodappdemomvpwithandroidx.view.home;


import com.example.ngocdroid.foodappdemomvpwithandroidx.model.Categories;
import com.example.ngocdroid.foodappdemomvpwithandroidx.model.Meals;

import java.util.List;

public interface HomeView {

    void showLoading();
    void hideLoading();
    void setMeal(List<Meals.Meal> meal);
    void setCategory(List<Categories.Category> category);
    void onErrorLoading(String message);

}
