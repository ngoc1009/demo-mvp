
package com.example.ngocdroid.foodappdemomvpwithandroidx.view.category;



import com.example.ngocdroid.foodappdemomvpwithandroidx.model.Meals;

import java.util.List;

public interface CategoryView {
    void showLoading();
    void hideLoading();
    void setMeals(List<Meals.Meal> meals);
    void onErrorLoading(String message);
}
