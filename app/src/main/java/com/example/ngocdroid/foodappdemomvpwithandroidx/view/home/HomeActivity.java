
package com.example.ngocdroid.foodappdemomvpwithandroidx.view.home;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.example.ngocdroid.foodappdemomvpwithandroidx.R;
import com.example.ngocdroid.foodappdemomvpwithandroidx.adapter.RecyclerViewHomeAdapter;
import com.example.ngocdroid.foodappdemomvpwithandroidx.adapter.ViewPagerHeaderAdapter;
import com.example.ngocdroid.foodappdemomvpwithandroidx.common.Utils;
import com.example.ngocdroid.foodappdemomvpwithandroidx.model.Categories;
import com.example.ngocdroid.foodappdemomvpwithandroidx.model.Meals;

import com.example.ngocdroid.foodappdemomvpwithandroidx.presenter.HomePresenter;
import com.example.ngocdroid.foodappdemomvpwithandroidx.view.category.CategoryActivity;
import com.example.ngocdroid.foodappdemomvpwithandroidx.view.detail.DetailActivity;

import java.io.Serializable;
import java.util.List;


import butterknife.BindView;
import butterknife.ButterKnife;


public class HomeActivity extends AppCompatActivity implements HomeView {

    public static final String EXTRA_CATEGORY = "category";
    public static final String EXTRA_POSITION = "position";
    public static final String EXTRA_DETAIL = "detail";

    @BindView(R.id.viewPagerHeader)
    ViewPager viewPagerMeal;
    @BindView(R.id.recyclerCategory)
    RecyclerView recyclerViewCategory;
    @BindView(R.id.edt_search)
    EditText edt_search;


    HomePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        presenter = new HomePresenter(this);
        presenter.getMeals();
        presenter.getCategories();
    }

    @Override
    public void showLoading() {
        findViewById(R.id.shimmerMeal).setVisibility(View.VISIBLE);
        findViewById(R.id.shimmerCategory).setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        findViewById(R.id.shimmerMeal).setVisibility(View.GONE);
        findViewById(R.id.shimmerCategory).setVisibility(View.GONE);
    }

    @Override
    public void setMeal(List<Meals.Meal> meal) {
        ViewPagerHeaderAdapter headerAdapter = new ViewPagerHeaderAdapter(meal, this);
        viewPagerMeal.setAdapter(headerAdapter);
        viewPagerMeal.setPadding(20, 0, 150, 0);
        headerAdapter.notifyDataSetChanged();

        headerAdapter.setOnItemClickListener((v, position) -> {

            TextView mealName = v.findViewById(R.id.mealName);
            Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
            intent.putExtra(EXTRA_DETAIL,mealName.getText().toString());
            startActivity(intent);

        });


        edt_search.setImeOptions(EditorInfo.IME_ACTION_DONE);
        edt_search.setRawInputType(InputType.TYPE_CLASS_TEXT);

        edt_search.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (edt_search.getText().toString().equals("")) {

                Utils.showDialogMessage(HomeActivity.this, "Title", "Please input keyword search !",R.drawable.ic_circle);

            } else  {




                        if (i == EditorInfo.IME_ACTION_NEXT || i == EditorInfo.IME_ACTION_DONE
                                || i == EditorInfo.IME_ACTION_SEARCH) {
                            Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
                            intent.putExtra(EXTRA_DETAIL,edt_search.getText().toString());
                            startActivity(intent);
                            return true;

                        }







            }
            return false;
        });


    }

    @Override
    public void setCategory(final List<Categories.Category> category) {
        RecyclerViewHomeAdapter homeAdapter = new RecyclerViewHomeAdapter(category, this);
        recyclerViewCategory.setAdapter(homeAdapter);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 3,
                GridLayoutManager.VERTICAL, false);
        recyclerViewCategory.setLayoutManager(layoutManager);
        recyclerViewCategory.setNestedScrollingEnabled(true);
        homeAdapter.notifyDataSetChanged();

        homeAdapter.setOnItemClickListener((view, position) -> {
            Intent intent = new Intent(getApplicationContext(), CategoryActivity.class);
            intent.putExtra(EXTRA_CATEGORY, (Serializable) category);
            intent.putExtra(EXTRA_POSITION, position);
            startActivity(intent);
        });
    }

    @Override
    public void onErrorLoading(String message) {
        Utils.showDialogMessage(this, "Title", message,R.drawable.ic_circle);
    }

}